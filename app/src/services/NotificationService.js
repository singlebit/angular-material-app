(function(){
  'use strict';
    angular.module('NotificationService', [])
         .service('NotificationService', [ NotificationService]);

  /**
   * Desktop Notifications Service
   *
   * @returns 
   * @constructor
   */
    function NotificationService() {

        function askNotification () {
            Notification.requestPermission().then(function (result) {
                console.log(result);
            });
        };
        function sendSimpleNotification (msg) {
            // Let's check if the browser supports notifications
            if (!("Notification" in window)) {
                alert("This browser does not support system notifications");
            }
    
                // Let's check whether notification permissions have already been granted
            else if (Notification.permission === "granted") {
                // If it's okay let's create a notification
                var notification = new Notification(msg);
            }
    
                // Otherwise, we need to ask the user for permission
            else if (Notification.permission !== 'denied') {
                Notification.requestPermission(function (permission) {
                    // If the user accepts, let's create a notification
                    if (permission === "granted") {
                        var notification = new Notification(msg);
                    }
                });
            }
    
            // Finally, if the user has denied notifications and you 
            // want to be respectful there is no need to bother them any more.
        };
    
        function spawnNotification(theBody, theIcon, theTitle) {
            var options = {
                body: theBody,
                icon: theIcon
            }
            var n = new Notification(theTitle, options);
            setTimeout(n.close.bind(n), 5000);
        }
    
       
        function closeNotification() { setTimeout(n.close.bind(n), 5000); }
      
    // Promise-based API
    return {
        spawnNotification: spawnNotification
        ,sendSimpleNotification:sendSimpleNotification
        ,askNotification: askNotification       
    };
  }

})();

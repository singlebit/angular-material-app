(function(){
  'use strict';

    angular.module('AccountService', ['ngResource'])
         .service('AccountService', ['$resource', AccountService]);

  /**
   * Users AccountService
   *
   * @returns {{login: Function, loggoff: Function}}
   * @constructor
   */
    function AccountService($resource) {

    var login = function (formdata) {
        //var request = $resource('externalLoginServiceUrl');
        return { Success: true ,Message: "Successful login"}; //TODO: request.save(formdata);
    };
    var logoff = function (formdata) {
        //var request = $resource('externalLogoffServiceUrl');
        return { Success: true ,Message: "Successful logoff"}; //TODO: request.save(formdata);
    };
    
    return {
        login: login,
        logoff: logoff
    };
  }

})();

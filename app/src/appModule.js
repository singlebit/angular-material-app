﻿/*
This is the main App Module and Controller.
All main configuration, routing and component loading is done here. 
*/
angular
    .module('starterApp',
    ['ngMaterial', 'first','second',
        'ngRoute', 'mainMenu', 'rightMenu',
        'toolbar', 'login', 'themePicker'
    ])
    .config(function ($mdThemingProvider, $mdIconProvider, $locationProvider, $routeProvider) {
        $mdIconProvider //https://github.com/nkoterba/material-design-iconsets/tree/master/iconsets
            .iconSet("call", 'assets/svg/communication-icons.svg', 24)
            .iconSet("action", 'assets/svg/action-icons.svg', 24)
            .iconSet("editor", 'assets/svg/editor-icons.svg', 24)
            .iconSet("content", 'assets/svg/content-icons.svg', 24)
            .iconSet("toggle", 'assets/svg/toggle-icons.svg', 24)
            .iconSet("social", 'assets/svg/social-icons.svg', 24)
            .iconSet("image", 'assets/svg/image-icons.svg', 24)
            .iconSet("file", 'assets/svg/file-icons.svg', 24)
            .iconSet("navigation", 'assets/svg/navigation-icons.svg', 24)
        
        var defaultColors = {
            primaryPalette: 'brown',
            accentPalette: 'orange'
        };
        $mdThemingProvider.theme('default')
            .primaryPalette(loadSavedPallete('primaryPalette'))
            .accentPalette(loadSavedPallete('accentPalette'));

        $locationProvider.hashPrefix('!');

        function loadSavedPallete(name) {
            var color = localStorage.getItem(name);
            return (color || defaultColors[name]);
        };

        //Define Routes
        $routeProvider
        .when('/first', {
            template: '<first></first>',
            requireLogin: true
        })
        .when('/second', {
            template: '<second></second>',
            requireLogin: true
        })
       .when('/themepicker', {
               template: '<theme-picker></theme-picker>',
               requireLogin: false
           })
        .when('/login', {
                 template: '<login></login>',
                 requireLogin: false
             })
        .when('/error', { template: '<h4 class="page-header">Error: path not found</h4>' })

        .otherwise('/first');
    })
/**
   * Main Controller for the Angular Material Starter App
   * @constructor
   */
   .controller('AppController', function () {
       var self = this;
       self.$onInit = function () {
    }

   });

(function () {
    'use strict';
    angular.module('first', ['ngMaterial', 'NotificationService']);
})();

(function () {
    angular
        .module('first')
        .component('first', {
            templateUrl: './src/first/first.html',
            controller: ('FirstController', [
                '$scope',
                'NotificationService',
                FirstController]
            )
        });

    /**
     * First Controller for the first component
     * @param $mdSidenav
     * @constructor
     */
    function FirstController($scope, NotificationService) {
        var self = this;

        self.sendSimpleNotification = function(){
            var msg = 'This is a test.';
            NotificationService.sendSimpleNotification(msg);
        };
       
    }

})();

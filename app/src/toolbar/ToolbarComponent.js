(function () {
    'use strict';
    angular.module('toolbar', ['ngMaterial']);
})();

(function () {
    angular
         .module('toolbar')
         .component('toolbar', {
             templateUrl: './src/toolbar/toolbar.html',
             controller: ('ToolbarController',[
                '$scope','$mdSidenav', '$mdDialog', '$location',
                ToolbarController]
             )}
         )
    ;

  /**
   * Main Menu Controller 
   * @constructor
   */
    function ToolbarController($scope, $mdSidenav, $mdDialog, $location) {
    var self = this;

    self.navigateTo = function (par, par2) {
        $location.path(par);
    };

    self.toggleToolbar = function (bar) {
        $mdSidenav(bar).toggle();
    }

    self.openMenu = function ($mdMenu, ev) {
        originatorEv = ev;
        $mdMenu.open(ev);
    };

    self.notificationsEnabled = true;
    self.toggleNotifications = function () {
        this.notificationsEnabled = !this.notificationsEnabled;
    };

    self.logOff = function () {
        $mdDialog.show(
          $mdDialog.alert()
            .targetEvent(originatorEv)
            .clickOutsideToClose(true)
            .parent('body')
            .title('Logoff')
            .textContent('You\'re about to be logged off...')
            .ok('OK')
           // .no('No')
        );
        $location.path('/first');
        originatorEv = null;
    };

    
  }

})();

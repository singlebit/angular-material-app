(function () {
    'use strict';
    angular.module('rightMenu', ['ngMaterial']);
})();

(function () {

    angular
         .module('rightMenu')
         .component('rightMenu', {
             templateUrl: './src/menu/rightMenu.html',
             controller: ('RightMenuController',[
             '$scope',
             '$mdSidenav', '$mdBottomSheet','$location',
             RightMenuController]
             )}
         )
    ;

  /**
   * Main Menu Controller 
   * @constructor
   */
    function RightMenuController($scope, $mdSidenav, $mdBottomSheet, $location) {
        var self = this;
        $scope.todos = [
    {
        face: 'assets/img/black_rabbit.png',
        what: 'Run',
        who: 'Morpheus',
        when: '2:08PM',
        notes: " I suggest you run"
    },
    {
        face: 'assets/img/white_rabbit.png',
        what: 'Knock knock',
        who: 'Trinity',
        when: '9:08PM',
        notes: " Follow the white rabbit "
    }, ];

    self.navigateTo = function (par, par2) {
        $location.path(par);
    };

    self.toggleMainMenu = function () {
        $mdSidenav('right').toggle();
    }
  }

})();

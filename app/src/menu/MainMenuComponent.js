(function () {
    'use strict';
    angular.module('mainMenu', ['ngMaterial']);
})();

(function () {
    angular
         .module('mainMenu')
         .component('mainMenu', {
             templateUrl: './src/menu/mainMenu.html',
             controller: ('MainMenuController',[
             '$scope',
             '$mdSidenav', '$mdDialog','$location',
             MainMenuController]
             )}
         )
    ;

    /**
     * Main Menu Controller 
     * @constructor
     */
    function MainMenuController($scope, $mdSidenav, $mdDialog, $location) {
        var self = this;

        self.navigateTo = function (par, par2) {
            $location.path(par);
        };
        self.openMenu = function ($mdMenu, ev) {
            originatorEv = ev;
            $mdMenu.open(ev);
        };
       
        self.toggleMainMenu = function () {
            $mdSidenav('left').toggle();
        }

    }
})();

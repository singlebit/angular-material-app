(function () {
    'use strict';
    angular.module('login', ['ngMaterial', 'AccountService']);
})();

(function () {
    angular
        .module('login')
        .component('login', {
            templateUrl: './src/account/login/login.html',
            controller: ('LoginController', [
                '$scope',
                'AccountService',
                '$location',
                '$mdDialog',
                LoginController]
            )
        }
        )
        ;

    /**
     * Login Controller 
     * @param $mdSidenav
     * @constructor
     */
    function LoginController($scope, AccountService, $location,$mdDialog) {
        var self = this;

        $scope.user = {};
        self.showResultStatusMessage = false;

        var last = {
            bottom: true,
            top: false,
            left: false,
            right: true
        };

        $scope.toastPosition = angular.extend({}, last);

        self.login = function (originatorEv) {
            var response = AccountService.login({ Email: $scope.user.Email, Password: $scope.user.Password });
            //.$promise.then(function (response) { //TODO: use when using actual $resource for external service
            response = response || { Success: false };
            if (response.Success) {
                $mdDialog.show(
                    $mdDialog.alert()
                      .targetEvent(originatorEv)
                      .clickOutsideToClose(true)
                      .parent('body')
                      .title('Login')
                      .textContent('Login Successful!')
                      .ok('OK')
                     // .no('No')
                  );
                  $location.path('/first');

            } else {
                $mdDialog.show(
                    $mdDialog.alert()
                        .targetEvent(originatorEv)
                        .clickOutsideToClose(true)
                        .parent('body')
                        .title('Login')
                        .textContent('Login Failed!')
                        .ok('OK')
                        // .no('No')
                    );
            }

        };
       
        self.$onInit = function () {

        };

    }

})();

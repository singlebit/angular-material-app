angular
    .module('themePicker', ['ngMaterial'])

    .component('themePicker', {
        templateUrl: './src/themepicker/themepicker.html',
        controller: ('ThemePickerController', [
            '$scope', '$mdColorPalette', '$location', '$mdToast',
            ThemePickerController]
        )
    }
    )

function ThemePickerController($scope, $mdColorPalette, $location, $mdToast) {
    $scope.colors = Object.keys($mdColorPalette);
    var theme_colors = [
        { name: 'red', color: 'rgb(244, 67, 54)' },
        { name: 'pink', color: 'rgb(233, 30, 99)' },
        { name: 'purple', color: 'rgb(156, 39, 176)' },
        { name: 'deep-purple', color: 'rgb(103, 58, 183)' },
        { name: 'indigo', color: 'rgb(63, 81, 181)' },
        { name: 'blue', color: 'rgb(33, 150, 243)' },
        { name: 'light-blue', color: 'rgb(3, 169, 244)' },
        { name: 'cyan', color: 'rgb(0, 188, 212)' },
        { name: 'teal', color: 'rgb(0, 150, 136)' },
        { name: 'green', color: 'rgb(76, 175, 80)' },
        { name: 'light-green', color: 'rgb(139, 195, 74)' },
        { name: 'lime', color: 'rgb(205, 220, 57)' },
        { name: 'yellow', color: 'rgb(255, 235, 59)' },
        { name: 'amber', color: 'rgb(255, 193, 7)' },
        { name: 'orange', color: 'rgb(255, 152, 0)' },
        { name: 'deep-orange', color: 'rgb(255, 87, 34)' },
        { name: 'brown', color: 'rgb(121, 85, 72)' },
        { name: 'grey', color: 'rgb(158, 158, 158)' },
        { name: 'blue-grey', color: 'rgb(96, 125, 139)' },
    ];

    $scope.setPrimary = false;
    $scope.primary = theme_colors[0].name;
    $scope.accent = theme_colors[1].name;
    
    $scope.colors = theme_colors.map(
        function (color) {
            return { name: color.name, color: color.color, designation: getDefaultDesignation(color.name) };
        });
    
        function getDefaultDesignation(color) {
        if (color == $scope.primary)
            return 'PRIMARY';
        if (color == $scope.accent)
            return 'ACCENT';

    };
    function showSimpleToast(message) {
        var pinTo = {
            bottom: false,
            top: true,
            left: false,
            right: true
        };

        $mdToast.show(
            $mdToast.simple()
                .textContent(message)
                .position(pinTo)
                .hideDelay(3000)
        );
    };

    self.load = function () {
        $scope.primary = localStorage.getItem('primaryPalette');
        $scope.accent = localStorage.getItem('accentPalette');
    }

    $scope.save = function () {
        if ($scope.primary == '' || $scope.accent == '') {
            showSimpleToast('Must select two colors');
            $scope.warning = "Must select two colors";
            return;
        }
        localStorage.setItem('primaryPalette', $scope.primary);
        localStorage.setItem('accentPalette', $scope.accent);
        showSimpleToast('Colors saved');
        $scope.warning = "Colors saved. Refreshing application...";
        setTimeout(function () { location.reload(); }, 2000);

    }
    $scope.setDesignation = function (color) {

        for (var i = 0; i < $scope.colors.length; i++) {
            if ($scope.colors[i].name == color & $scope.colors[i].designation == '') {
                $scope.colors[i].designation = $scope.setPrimary ? 'PRIMARY' : 'ACCENT';
                if ($scope.setPrimary) $scope.primary = color;
                else $scope.accent = color;
            }
            else if ($scope.colors[i].designation != 'ACCENT' && $scope.setPrimary)
                $scope.colors[i].designation = '';
            else if ($scope.colors[i].designation != 'PRIMARY' && !$scope.setPrimary)
                $scope.colors[i].designation = '';
        }
        $scope.setPrimary = !$scope.setPrimary;
    };
    $scope.isPrimary = true;
    $scope.myStyle = function () { return '{ color: "red"; font-size: "xx-large"}' };
    $scope.selectTheme = function (color) {
        if ($scope.isPrimary) {
            $scope.primary = color;

            $scope.isPrimary = false;
        }
        else {
            $scope.accent = color;

            $scope.isPrimary = true;
        }
    };
    self.$onInit = function () {
        self.load();
    };
}
(function () {
    'use strict';
    angular.module('second', ['ngMaterial']);
})();

(function () {
    angular
        .module('second')
        .component('second', {
            templateUrl: './src/second/second.html',
            controller: ('SecondController', [
                '$scope',
                SecondController]
            )
        });

    /**
     * Second Controller for the second component
     * @constructor
     */
    function SecondController($scope) {
        var self = this;

        self.selected = null;
        self.second = [];

    }

})();
